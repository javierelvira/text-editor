import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import java.awt.BorderLayout;
import java.awt.Scrollbar;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class Principal {

	private static JFrame frame;
	private static JTextPane txtpnescribeAqu;
	private JFileChooser fc = new JFileChooser();
	static Preferencias pref = new Preferencias();
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public void abrirArchivo() {
		//abrimos opciones y la guardamos en opcion
		int opcion=fc.showOpenDialog(frame);
		//Si pincha en aceptar
		if (opcion==JFileChooser.APPROVE_OPTION) 
			{
				//seleccionamos el fichero
				File fichero=fc.getSelectedFile();
			
				try
				{
					FileReader fr = new FileReader(fichero);
					String cadena="";
					int valor=fr.read();
					while(valor!=-1) {
						cadena = cadena+(char)valor;
						valor = fr.read();
					}
					txtpnescribeAqu.setText(cadena);
				
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}

	
	public void guardarArchivo() {

		try{
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Guardar");
			chooser.showOpenDialog(null);
			String text=txtpnescribeAqu.getText().toString();
			File fichero= chooser.getSelectedFile();
			
			if(fichero != null) {
				FileWriter fileWriter = new FileWriter(fichero);
				BufferedWriter bw = new BufferedWriter(fileWriter);
				bw.write(text);
				bw.close();			
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void NuevoArchivo() {		
		txtpnescribeAqu.setText("");
	}
	
	
	public void abrirPreferencias() {
		pref.getFrame().setVisible(true);
    }
	
	public static void cambiarPreferencias() {
		int color = pref.getColor();
		if (color == 1) {
			frame.getContentPane().setBackground(new Color(211, 210, 217));
			txtpnescribeAqu.setBackground(new Color(255,255,255));
			txtpnescribeAqu.setForeground(Color.BLACK);
		}
		else if(color == 2) {
			frame.getContentPane().setBackground(new Color(52, 52, 55));
			txtpnescribeAqu.setBackground(new Color(68, 67, 77));
			txtpnescribeAqu.setForeground(Color.WHITE);
		}
	}
	
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(211, 210, 217));
		frame.setResizable(false);
		frame.setBounds(100, 100, 1280, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 20, 1220, 640);
		frame.getContentPane().add(scrollPane);
		
		txtpnescribeAqu = new JTextPane();
		txtpnescribeAqu.setText("\"Escribe aqu\u00ED\"");
		txtpnescribeAqu.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtpnescribeAqu.setBackground(new Color(255,255,255));
		txtpnescribeAqu.setForeground(Color.BLACK);
		scrollPane.setViewportView(txtpnescribeAqu);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(20, 2, 70, 20);
		frame.getContentPane().add(menuBar);
		
		JMenu mnOpciones = new JMenu("Opciones");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmNuevoArchivo = new JMenuItem("Nuevo Archivo");
		mntmNuevoArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NuevoArchivo();
			}
		});
		mnOpciones.add(mntmNuevoArchivo);
		
		JMenuItem mntmAbrirArchivo = new JMenuItem("Abrir Archivo");
		mntmAbrirArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirArchivo();
			}
		});
		mnOpciones.add(mntmAbrirArchivo);
		
		JMenuItem mntmGuardarArchivo = new JMenuItem("Guardar Archivo");
		mntmGuardarArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarArchivo();
			}
		});
		mnOpciones.add(mntmGuardarArchivo);
		
		JMenuItem mntmPreferencias = new JMenuItem("Preferencias");
		mntmPreferencias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				abrirPreferencias();
			}
		});
		mnOpciones.add(mntmPreferencias);
	}
}








