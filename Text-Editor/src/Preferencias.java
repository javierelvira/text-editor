import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;

public class Preferencias {

	private static JFrame frame;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private int colour;
	static Principal prin = new Principal();
	public JFrame getFrame() {
		return frame;
	}
	public void setFrame(JFrame jframe) {
		this.frame = jframe;
	}
	public void setColor(int color) {
		this.colour = color;
	}
	public int getColor() {
		return colour;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Preferencias window = new Preferencias();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Preferencias() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setLayout(null);
		
		JRadioButton rdbtnTemaClaro_1 = new JRadioButton("Tema Claro");
		rdbtnTemaClaro_1.setSelected(true);
		buttonGroup.add(rdbtnTemaClaro_1);
		rdbtnTemaClaro_1.setBounds(36, 7, 109, 23);
		frame.getContentPane().add(rdbtnTemaClaro_1);
		
		JRadioButton rdbtnTemaOscuro_1 = new JRadioButton("Tema Oscuro");
		buttonGroup.add(rdbtnTemaOscuro_1);
		rdbtnTemaOscuro_1.setBounds(36, 43, 109, 23);
		rdbtnTemaOscuro_1.setSelected(false);
		frame.getContentPane().add(rdbtnTemaOscuro_1);
		frame.setBounds(100, 100, 200, 150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnTemaClaro_1.isSelected()) {
					setColor(1);
				}
				else if (rdbtnTemaOscuro_1.isSelected()) {
					setColor(2);
				}
				frame.setVisible(false);
				prin.cambiarPreferencias();
			}
		});
		btnAceptar.setBounds(43, 69, 89, 23);
		frame.getContentPane().add(btnAceptar);
	}
}
